<!DOCTYPE html>
<html>
<head>
  <title>Test 2</title>

  <style type="text/css">
.mdl-layout {
  align-items: center;
  justify-content: center;
}
.mdl-layout__content {
  padding: 24px;
  flex: none;
}
  </style>

  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
</head>


<body>
<div class="mdl-layout mdl-js-layout mdl-color--grey-100">
  <main class="mdl-layout__content" style="margin-left: 808px; margin-top: 113px;">
    <div class="mdl-card mdl-shadow--6dp">
      <div class="mdl-card__title mdl-color--primary mdl-color-text--white">
        <h2 class="mdl-card__title-text">Bitjini.</h2>
      </div>
      <div class="mdl-card__supporting-text">
        <form action="#">
          <div class="mdl-textfield mdl-js-textfield">
            <input class="mdl-textfield__input" type="text" id="username" />
            <label class="mdl-textfield__label" for="username">Username</label>
          </div>
          <div class="mdl-textfield mdl-js-textfield">
            <input class="mdl-textfield__input" type="password" id="userpass" />
            <label class="mdl-textfield__label" for="userpass">Password</label>
          </div>
        </form>
      </div>
      <div class="mdl-card__actions ">
        <button class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">Log in</button>
      </div>
    </div>
  </main>
</div>



</body>
</html>